const students = [
	{
		name: "Nam",
		age: 24,
		gender: "male"
	},
	{
		name: "Mai",
		age: 22,
		gender: "female"
	},
	{
		name: "Trang",
		age: 23,
		gender: "female"
	},
	{
		name: "An",
		age: 20,
		gender: "male"
	},
	{
		name: "Thien",
		age: 27,
		gender: "male"
	}
];

var numMales = students.reduce(function(n, person) {
	return n + (person.gender == "male");
}, 0);

console.log(`The number of male students is: ${numMales} students`);
console.log(
	`The number of female students is: ${students.length - numMales} students`
);

return { numberOfMales: numMales, numberOfFemales: students.length - numMales };
