const fs = require("fs");
const dateFns = require("date-fns");
const formatDistance = require("date-fns/formatDistance");
const viLocale = require("date-fns/locale/vi");

const formatMoney = (money) => {
	return `${money.toFixed(0).replace(/\d(?=(\d{3})+$)/g, "$&,")}VND`;
};

const distanceFromNow = (dateUpdated) => {
	return formatDistance(new Date(dateUpdated), new Date(), {
		locale: viLocale
	});
};

fs.readFile("./product.json", "utf8", function(err, data) {
	if (err) {
		console.log("err ", err);
	}

	const products = JSON.parse(data.toString());

	console.log("Total number of products ", products.length);

	// Convert dateUpdated of each item into real Date.
	const updatedProducts = products.map((p) => ({
		...p,
		dateUpdated: new Date(p.dateUpdated)
	}));

	updatedProducts.forEach((product) => {
		const price = formatMoney(product.price);
		const fromNow = distanceFromNow(product.dateUpdated);
		console.log(
			`${product.id} - ${product.name} - ${price} - Cập nhật cách đây: ${fromNow}`
		);
	});
});
