const readline = require("readline");

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.question("What's your name? ", (answer) => {
	name = answer;
	rl.question("What's your year of birth? ", (answer) => {
		birthYear = answer;
		rl.question("What's your home town? ", (answer) => {
			homeTown = answer;
			rl.close();
		});
	});
});

rl.on("close", () => {
	console.log(
		`Thank you. Hello ${name}, so you are ${new Date().getFullYear() -
			birthYear} year old and from ${homeTown}.`
	);
});
